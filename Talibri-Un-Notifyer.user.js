// ==UserScript==
// @name         Talibri Un-Notifyer
// @namespace    http://gitlab.com/Talibri
// @version      0.0.1
// @description  No more notifications!
// @author       Dan Campbell
// @match        https://talibri.com/*
// @grant        none
// @require  https://gist.github.com/raw/2625891/waitForKeyElements.js
// ==/UserScript==

function Unnotify() {
    'use strict';

    clearNotifications();

}


waitForKeyElements(".main-page", Unnotify);
